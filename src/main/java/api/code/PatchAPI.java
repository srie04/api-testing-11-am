package api.code;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class PatchAPI {

	
	
	public CloseableHttpResponse patch(String url, String responseBody) throws ClientProtocolException, IOException {
		
		CloseableHttpClient httpClient = HttpClients.createDefault();
		
		HttpPatch httpPatch = new HttpPatch(url);
		
		httpPatch.setHeader("Content-Type", "application/json");
		
		
		HttpEntity entity = new StringEntity(responseBody, ContentType.APPLICATION_JSON);
		httpPatch.setEntity(entity);
		
		CloseableHttpResponse patchResponse = httpClient.execute(httpPatch);

		
		return patchResponse;
	}
}
