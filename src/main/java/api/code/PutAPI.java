package api.code;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class PutAPI {

	public CloseableHttpResponse put(String url, String jsonBody) throws ClientProtocolException, IOException {
		
		//Make a connection. 
		CloseableHttpClient httpClient = HttpClients.createDefault();

		//URL,
		HttpPut httpPut = new HttpPut(url);
		//Headers
		httpPut.setHeader("Content-Type", "application/json");
		
		//Json Body
		
		HttpEntity entity = new StringEntity(jsonBody);
		
		httpPut.setEntity(entity);
		
		CloseableHttpResponse putResponse = httpClient.execute(httpPut);
		
		return putResponse;
		
	}
}
