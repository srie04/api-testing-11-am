package api.code;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class PostAPI {

	
	public CloseableHttpResponse post(String url, String requestBody) throws ClientProtocolException, IOException {
		
		//HTTP Connection
		CloseableHttpClient httpClient = HttpClients.createDefault();
		
		//Need to pass a URL
		HttpPost httpPost = new HttpPost(url);
		
		//set headers
		httpPost.setHeader("Content-Type", "application/json");
		
		//set the Json body 
		HttpEntity entity = new StringEntity((requestBody), ContentType.APPLICATION_JSON);
		
		httpPost.setEntity(entity);
		
		//Send the request
		CloseableHttpResponse postResponse = httpClient.execute(httpPost);
		
		return postResponse;
	}
}
