package api.code;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class HeadAPI {

	
	
	public CloseableHttpResponse head(String url) throws ClientProtocolException, IOException {
		
		
		CloseableHttpClient httpClient = HttpClients.createDefault();
		
		HttpHead httHead = new HttpHead(url);
		
		CloseableHttpResponse headResponse = httpClient.execute(httHead);
		
		return headResponse;
		
	}
	
	
}
