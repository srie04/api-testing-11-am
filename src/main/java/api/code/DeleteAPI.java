package api.code;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class DeleteAPI {

	
	public CloseableHttpResponse delete(String url) throws ClientProtocolException, IOException {
		
		CloseableHttpClient httpClient = HttpClients.createDefault();
		
		HttpDelete httpDelete = new HttpDelete(url);
		
		CloseableHttpResponse deleteResponse = httpClient.execute(httpDelete);
		
		return deleteResponse;
	}
}
