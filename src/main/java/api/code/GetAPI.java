package api.code;


import java.io.IOException;
import java.util.HashMap;

import org.apache.http.Header;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

public class GetAPI {

	
	
	public void get(String url) throws ClientProtocolException, IOException {
	
		//API TESTING CODE 
		
		//To make a connection to API 
		CloseableHttpClient httpClient = HttpClients.createDefault();
		
		HttpGet httpGet = new HttpGet(url);
		
		//this wil execute and hold the response
		CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
		//Status
		int statusCode = httpResponse.getStatusLine().getStatusCode();
		
		System.out.println("Status Code ---> "+statusCode);
		//BODY --> String
		String string = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
		
		//STring -- JSON
		
		JSONObject responseJSON = new JSONObject(string);
		
		System.out.println("JSON VALUES ---> "+ responseJSON);
		
		//Headers
		Header[] allHeaders = httpResponse.getAllHeaders();
		
		HashMap<String, String> headerMap = new HashMap<String, String>();
		
		for (Header header : allHeaders) {
			headerMap.put(header.getName(), header.getValue());
		}
		
		System.out.println("HEADERS ---> " +headerMap);
		
		
	}
	
	
	
	
	
	
}
