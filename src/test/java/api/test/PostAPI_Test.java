package api.test;

import java.io.File;
import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.exc.StreamWriteException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;

import api.base.API_Base;
import api.base.Users;
import api.code.PostAPI;

public class PostAPI_Test {

	
	String url ;
	
	@BeforeMethod
	public void getUrl() {
		API_Base base = new API_Base();
		String appURL = base.prop.getProperty("url");
		String serviceURL = base.prop.getProperty("apiUrl");
		 url = appURL + serviceURL;
	}
	
	
	@Test
	public void postApi() throws StreamWriteException, DatabindException, IOException {
		// Creating object for the Base class
		PostAPI postApi = new PostAPI();
		
		//Dependancy - Jackson API
		ObjectMapper mapper = new ObjectMapper(); // This will change the user input into Json file
		
		//Creating Object for Users Class
		Users users = new Users("Naveen", "Hero");
		
		//Convert the users given value into a json
		mapper.writeValue(new File("C:\\Users\\Santhosh\\eclipse-workspace\\API_Testing\\Configuration\\users.json"), users);
		
		//Convert the json values to string
		String writeValueAsString = mapper.writeValueAsString(users);
		
		//Pass the values in post method
		CloseableHttpResponse post = postApi.post(url, writeValueAsString);
		
		//Get the status code
		int statusCode = post.getStatusLine().getStatusCode();
		System.out.println("Status Code --> " + statusCode);
		
		//Get the response Json body
		HttpEntity entity = post.getEntity();
		
		String reponseBody = EntityUtils.toString(entity);
		
		System.out.println(reponseBody);
		
	}
	
	
	
	
}
