package api.test;

import java.io.IOException;
import java.util.HashMap;

import org.apache.http.Header;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import api.base.API_Base;
import api.code.HeadAPI;
import api.code.OptionAPI;

public class OptionsAPI_Test {
	String url;

	@BeforeMethod
	public void getUrl() {
		API_Base base = new API_Base();
		String appURL = base.prop.getProperty("url");
		String serviceURL = base.prop.getProperty("apiUrl");
		url = appURL + serviceURL;
	}

	@Test
	public void optionsAPI() throws ClientProtocolException, IOException {

		OptionAPI options = new OptionAPI();

		CloseableHttpResponse response = options.options(url);

		int statusCode = response.getStatusLine().getStatusCode();

		System.out.println("Status Code --> " + statusCode);

//		String string = EntityUtils.toString(response.getEntity());
//
//		System.out.println("Response Body --> " + string);

		Header[] allHeaders = response.getAllHeaders();

		HashMap<String, String> headerMap = new HashMap<String, String>();

		for (Header header : allHeaders) {

			headerMap.put(header.getName(), header.getValue());
		}

		System.out.println("Headers ---> " + headerMap);

	}
}
