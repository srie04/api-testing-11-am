package api.test;

import java.io.IOException;
import java.util.HashMap;

import org.apache.http.Header;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import api.base.API_Base;
import api.code.DeleteAPI;

public class DeleteAPI_Test {

	String url;

	@BeforeMethod
	public void getUrl() {
		API_Base base = new API_Base();
		String appURL = base.prop.getProperty("url");
		String serviceURL = base.prop.getProperty("apiUrl");
		url = appURL + serviceURL;
	}

	@Test
	public void deleteAPI() throws ClientProtocolException, IOException {
		DeleteAPI delete = new DeleteAPI();

		CloseableHttpResponse deleteReponse = delete.delete(url);

		int statusCode = deleteReponse.getStatusLine().getStatusCode();

		System.out.println(statusCode);

		Header[] allHeaders = deleteReponse.getAllHeaders();

		HashMap<String, String> headerMap = new HashMap<String, String>();

		for (Header header : allHeaders) {

			headerMap.put(header.getName(), header.getValue());
		}

		System.out.println("Headers ---> " + headerMap);

	}
}
