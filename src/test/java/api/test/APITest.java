package api.test;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import api.base.API_Base;
import api.code.GetAPI;

public class APITest {
	String url ;
	
	@BeforeMethod
	public void getUrl() {
		API_Base base = new API_Base();
		String appURL = base.prop.getProperty("url");
		String serviceURL = base.prop.getProperty("apiUrl");
		 url = appURL + serviceURL;
	}
	
	
	
	@Test
	public void getAPITest() throws ClientProtocolException, IOException {
		GetAPI get = new GetAPI();
		get.get(url);

	}
	
}
