package api.test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.exc.StreamWriteException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;

import api.base.API_Base;
import api.base.Users;
import api.code.PatchAPI;

public class PatchAPI_Test {

	String url;

	@BeforeMethod
	public void getUrl() {
		API_Base base = new API_Base();
		String appURL = base.prop.getProperty("url");
		String serviceURL = base.prop.getProperty("apiUrl");
		url = appURL + serviceURL;
	}

	@Test
	public void patch() throws StreamWriteException, DatabindException, IOException {

		PatchAPI patchAPI = new PatchAPI();

		ObjectMapper mapper = new ObjectMapper();

		Users users = new Users("RandyOrten", "RAW");

		mapper.writeValue(
				new File("C:\\Users\\Santhosh\\eclipse-workspace\\API_Testing\\Configuration\\patchUsers.json"), users);

		String writeValueAsString = mapper.writeValueAsString(users);

		CloseableHttpResponse response = patchAPI.patch(url, writeValueAsString);

		
		int statusCode = response.getStatusLine().getStatusCode();
		
		System.out.println("Status code --> "+statusCode);
		
		String response_body = EntityUtils.toString(response.getEntity());
		
		System.out.println("Json response ---> "+response_body);
		
		
	Header[] allHeaders = response.getAllHeaders();
		
		HashMap<String, String> headerMap = new HashMap<String, String>();
		
		for (Header header : allHeaders) {
			
			headerMap.put(header.getName(), header.getValue());
		}
		
		System.out.println("Headers ---> " +headerMap );
		
		
		
		
		
		
		
	}
}
